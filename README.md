# My portfolio

## Summary

A simple Ruby-Sinatra app to show the world your marvellous skills!


Test the live demo (my website) in [http://portfolio.danielherzog.es/](http://portfolio.danielherzog.es/ "portfolio").

## Usage
Install the app by using bundler:

````sh
$ bundler install
````

And run it with rake tasks:

````sh
$ rake
// ...
$ rake -T
// List all tasks and their description.
````

To configure the email notifier (contact form), you must set up the following enviromental variables to access a valid email account:

- *mail_user*: Email username. Depends on your service (for example, info.example.com (or info) from example.com).
- *mail_password*: Email password. It's okey, we are no sending data from a backdoor. Check it out in the source code :)
- *mail_server*: Your email **smtp** server (for example, smtp.example.com).
- *mail_por*: Por to access the mail server. It's usually 587.
- *mail_dest*: You need a mailbox for your emails! So, set up the receiver account in this var. Note that you can (and should) send messages to yourself.

## Dependencies
Dependencies can be found on the `Gemfile`.

Made with [Freelancer](http://startbootstrap.com/template-overviews/freelancer/) template.

## TODO

- Move the projects data json to a database.
- Move the CV file to an external storage service (like Dropbox).

## Authors

This project has been developed by:

| Avatar  | Name          | Nickname  | Email              |
| ------- | ------------- | --------- | ------------------ |
| ![](http://i59.tinypic.com/fasosx.jpg)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com)
helpers do
  def projects_data
    @projects_data ||= JSON.parse File.read("data/projects_data/#{I18n.locale}.json").force_encoding('UTF-8')
  end

  def link(path, locale = I18n.locale)
    "#{path}#{path.index('?') ? '&' : '?'}locale=#{locale}"
  end

  def t(*args)
    I18n.t *args
  end
end

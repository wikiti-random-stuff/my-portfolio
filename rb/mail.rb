require 'mail'

# --------------------------
#     Correo electrónico
# --------------------------

# Cargar datos
Mail.defaults do
  delivery_method :smtp, {
    address: ENV['mail_server'],
    port: ENV['mail_port'],
    user_name: ENV['mail_user'],
    password: ENV['mail_password'],
    openssl_verify_mode: 'none'
  }
end

# Preparar uri
post '/mail' do
  name = params['name']
  email = params['email']
  phone = params['phone']
  message = params['message']

  return false if name.to_s == "" || email.to_s == "" || message.to_s == ""

  tsubject = %{[NOTIFICACIÓN PORTFOLIO] Mensaje de #{email}}
  tbody = %(
    <h3>Datos de contacto</h3>
    <ul>
      <li><strong>Nombre: </strong>#{name}</li>
      <li><strong>Email: </strong>#{email}</li>
      <li><strong>Teléfono: </strong>#{phone}</li>
    </ul>

    <h3>Contenido del mensaje</h3>
    <p>#{message.gsub "\n", "<br />"}</p>
  )

  Mail.deliver do
    reply_to email
    to ENV['mail_dest']
    from ENV['mail_user']
    subject tsubject

    html_part do
      content_type 'text/html; charset=UTF-8'
      body tbody
    end
  end

  return true
end

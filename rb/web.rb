before do
  I18n.locale = params[:locale] if params[:locale] && I18n.available_locales.include?(params[:locale].to_sym)
end

get '/' do
  locale = request.env['HTTP_ACCEPT_LANGUAGE'].to_s.scan(/^[a-z]{2}/).first || I18n.default_locale
  redirect to(link '/home')
end

get '/:path/?' do |p|
  erb :"/#{p}", layout: :'/layout', locals: { path: '/' }
end

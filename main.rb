# encoding: UTF-8

require 'sinatra'
require 'json'
require 'i18n'

require_relative 'rb/mail'
require_relative 'rb/web'
require_relative 'rb/helpers'

I18n.available_locales = %i(es en)
I18n.default_locale = I18n.available_locales.first
I18n.load_path.concat(I18n.available_locales.map { |l| "data/locales/#{l}.yml" })

set :public_folder, './public'
